import React, {Component} from 'react';

const API_KEY = "6d3d4f035947d7a4280824f3a8658b05";
class FetchData extends Component{
    constructor(){
        super()
        this.state = {
            value : 'Almaty',
            loading : true,
            weatherData : {}
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)        
    }
    componentDidMount(){
        this.fetchingDataOnClick();
    }
    fetchingDataOnClick(){
        this.setState({loading : false, weatherData : {}})  
        fetch(`http://api.openweathermap.org/data/2.5/weather?q=${this.state.value},kz&appid=${API_KEY}`)
        .then(response => response.json())
        .then(data => {
            console.log(data);
            this.setState({
                loading : false,
                weatherData : {
                    city : data.name,
                    country : data.sys.country,
                    temp : Math.floor(data.main.temp - 273.15),
                    feels_like : Math.floor(data.main.feels_like - 273.15),
                    temp_min : Math.floor(data.main.temp_min - 273.15),
                    temp_max : Math.floor(data.main.temp_max - 273.15),
                    weather_description : data.weather[0].description,
                    main : data.weather[0].main,
                    humidity : data.main.humidity,
                    wind : data.wind.speed,
                } 
            })
        })
    }
    handleSubmit(event){
        console.log('Input clicked.');
        this.fetchingDataOnClick();
        event.preventDefault();
    }
    handleChange(event){
        console.log(event.target.name)
        console.log(event.target.value)
        this.setState({
            value : event.target.value
        })
    }
    render(){
        return(
            <div>
                <form onClick={this.handleSubmit}>
                    <label>
                        Please select city: 
                        <select value={this.state.value} onChange={this.handleChange}>
                            <option value="Almaty">Almaty</option>
                            <option value="Taraz">Taraz</option>
                            <option value="Shymkent">Shymkent</option>
                            <option value="Astana">Nur-Sultan</option>
                            <option value="Karaganda">Karaganda</option>
                        </select>
                    </label>
                    <input type="submit" value="Submit" />
                </form>

                <h1>Fetching the data</h1>
                {this.state.loading ? <h4>Fetching status: Loading</h4> : <h4>Fetching status: Completed</h4>}
                <h1>{this.state.weatherData.city}</h1> 
                <h1>{this.state.weatherData.country}</h1> 
                <h1>{this.state.weatherData.temp}°C</h1> 
                <h1>{this.state.weatherData.feels_like}°C</h1> 
                <h1>{this.state.weatherData.temp_min}°C</h1> 
                <h1>{this.state.weatherData.temp_max}°C</h1>    
                <h1>{this.state.weatherData.weather_description}</h1>    
                <h1>{this.state.weatherData.main}</h1>                                          
                <h1>{this.state.weatherData.humidity} %</h1>
                <h1>{this.state.weatherData.wind} km/h</h1>
            </div>
        )
    }
}
;
export default FetchData;