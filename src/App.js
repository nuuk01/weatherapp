import React from 'react';
import './App.css';
import FetchData from './FetchData';

function App() {
  return (
    <div className="App">
      <h1>Weather App</h1>
      <FetchData />
    </div>
  );
}

export default App;
